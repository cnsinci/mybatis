package com.lagou.test;

import com.lagou.builder.SqlSessionFactoryBuilder;
import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;

import java.io.InputStream;
import java.util.List;

public class Test {

    @org.junit.Test
    public void test1() throws Exception {
        InputStream inputStream = Resources.getResourcesAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().builder(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        List<User> users = userDao.selectList();
        for (User user : users) {
            System.out.println(user);
        }
        sqlSession.close();
    }

    @org.junit.Test
    public void test2() throws Exception {
        InputStream inputStream = Resources.getResourcesAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().builder(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        User queryUser = new User();
        queryUser.setId(2L);
        queryUser.setUsername("tom");
        User user = userDao.selectOne(queryUser);
        System.out.println(user);
        sqlSession.close();
    }

    @org.junit.Test
    public void testInsert() throws Exception {
        //读取配置文件
        InputStream inputStream = Resources.getResourcesAsStream("sqlMapConfig.xml");
        //解析配置文件封装configuration mapperStatement,创建sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().builder(inputStream);
        //获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();
        // 获取代理对象
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        User user = new User();
        user.setUsername("sinci");
        userDao.insert(user);
        sqlSession.close();
    }

    @org.junit.Test
    public void testUpdate() throws Exception {
        InputStream inputStream = Resources.getResourcesAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().builder(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        User user = new User();
        user.setId(4L);
        user.setUsername("sinci-update");
        userDao.update(user);
        sqlSession.close();
    }

    @org.junit.Test
    public void testDelete() throws Exception {
        InputStream inputStream = Resources.getResourcesAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().builder(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSqlSession();

        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        User user = new User();
        user.setId(4L);
        user.setUsername("sinci-update");
        userDao.delete(user);
        sqlSession.close();
    }
}
