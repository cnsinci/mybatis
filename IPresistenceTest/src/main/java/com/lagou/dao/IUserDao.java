package com.lagou.dao;

import com.lagou.pojo.User;

import java.util.List;

public interface IUserDao {

    List<User> selectList() throws Exception;

    User selectOne(User user) throws Exception;

    void insert(User user);

    void update(User user);

    void delete(User user);
}
