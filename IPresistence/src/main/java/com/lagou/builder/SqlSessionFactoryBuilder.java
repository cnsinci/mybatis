package com.lagou.builder;

import com.lagou.pojo.Configuration;
import com.lagou.sqlSession.DefaultSqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactory;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * @author Administrator
 * @date 2020/12/10 13:52
 */
public class SqlSessionFactoryBuilder {
    private Configuration configuration;

    public SqlSessionFactoryBuilder() {
        this.configuration = new Configuration();
    }

    public SqlSessionFactory builder(InputStream inputStream) throws DocumentException, PropertyVetoException, ClassNotFoundException {

        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder(configuration);
        xmlConfigBuilder.parseConfiguration(inputStream);

        return new DefaultSqlSessionFactory(configuration);
    }
}
