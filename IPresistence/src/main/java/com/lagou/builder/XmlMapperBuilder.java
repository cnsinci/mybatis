package com.lagou.builder;

import com.lagou.enums.MapperType;
import com.lagou.pojo.Configuration;
import com.lagou.pojo.MapperStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * @author 解析Mapper.xml的配置信息，保存到Configuration中的MapperStatement中
 * @date 2020/12/10 14:01
 */
public class XmlMapperBuilder {
    private Configuration configuration;

    public XmlMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    public void parseStatement(InputStream inputStream) throws DocumentException, ClassNotFoundException {
        Document document = new SAXReader().read(inputStream);
        Element rootElement = document.getRootElement();
        String namespace = rootElement.attributeValue("namespace");
        //组装select标签Statement
        List<Element> selectList = rootElement.selectNodes("select");
        buildStatement(selectList, namespace, MapperType.SELECT);
        //组装insert标签Statement
        List<Element> insertList = rootElement.selectNodes("insert");
        buildStatement(insertList, namespace, MapperType.INSERT);
        //组装update标签Statement
        List<Element> updateList = rootElement.selectNodes("update");
        buildStatement(updateList, namespace, MapperType.UPDATE);
        //组装delete标签Statement
        List<Element> deleteList = rootElement.selectNodes("delete");
        buildStatement(deleteList, namespace, MapperType.DELETE);

    }

    private void buildStatement(List<Element> mapperList, String namespace, MapperType mapperType) throws ClassNotFoundException {
        for (Element element : mapperList) {
            //namespace.id组成唯一标识statementId
            String statementId = namespace + "." + element.attributeValue("id");
            MapperStatement mapperStatement = new MapperStatement();
            mapperStatement.setMapperType(mapperType);
            mapperStatement.setId(statementId);
            mapperStatement.setSql(element.getTextTrim());
            String paramType = element.attributeValue("paramType");
            if (null != paramType) {
                mapperStatement.setParamClazz(Class.forName(paramType));
            }
            String resultType = element.attributeValue("resultType");
            if (null != resultType) {
                mapperStatement.setResultClazz(Class.forName(resultType));
            }

            configuration.getMapperStatementMap().put(statementId, mapperStatement);
        }
    }
}
