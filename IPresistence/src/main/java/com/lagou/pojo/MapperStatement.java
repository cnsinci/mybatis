package com.lagou.pojo;

import com.lagou.enums.MapperType;

/**
 * @author Administrator
 * @date 2020/12/10 13:48
 */
public class MapperStatement {
    private String id;
    private String sql;
    private Class<?> paramClazz;
    private Class<?> resultClazz;
    private MapperType mapperType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Class<?> getParamClazz() {
        return paramClazz;
    }

    public void setParamClazz(Class<?> paramClazz) {
        this.paramClazz = paramClazz;
    }

    public Class<?> getResultClazz() {
        return resultClazz;
    }

    public void setResultClazz(Class<?> resultClazz) {
        this.resultClazz = resultClazz;
    }

    public MapperType getMapperType() {
        return mapperType;
    }

    public void setMapperType(MapperType mapperType) {
        this.mapperType = mapperType;
    }
}
