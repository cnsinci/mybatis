package com.lagou.io;

import java.io.InputStream;

/**
 * @author Administrator
 * @date 2020/12/10 13:46
 */
public class Resources {
    public static InputStream getResourcesAsStream(String path) {
        return Resources.class.getClassLoader().getResourceAsStream(path);
    }
}
