package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;

/**
 * @author Administrator
 * @date 2020/12/10 14:02
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSqlSession() {
        return new DefaultSqlSession(configuration);
    }
}
