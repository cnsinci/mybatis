package com.lagou.sqlSession;

/**
 * @author Administrator
 * @date 2020/12/10 13:54
 */
public interface SqlSessionFactory {
    public SqlSession openSqlSession();
}
