package com.lagou.sqlSession;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Sinci.Wang
 * @date 2020/12/10 14:44
 */
public interface SqlSession {
    /**
     * 查询list
     */
    public <E> List<E> selectList(String statementId, Object... params) throws Exception;

    /**
     * 查询单个对象
     */
    public <T> T selectOne(String statementId, Object... params) throws Exception;

    int insert(String statementId, Object... params) throws Exception;

    int update(String statementId, Object... params) throws Exception;

    int delete(String statementId, Object... params) throws Exception;

    /**
     * 获取mapper的代理对象
     */
    public <T> T getMapper(Class<?> mapperClazz) throws Exception;

    /**
     * 关闭sqlSession
     */
    public void close() throws SQLException;
}
