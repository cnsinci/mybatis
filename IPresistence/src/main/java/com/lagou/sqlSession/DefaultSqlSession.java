package com.lagou.sqlSession;

import com.lagou.enums.MapperType;
import com.lagou.executor.Executor;
import com.lagou.executor.SimpleExecutor;
import com.lagou.pojo.Configuration;
import com.lagou.pojo.MapperStatement;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Administrator
 * @date 2020/12/10 14:45
 */
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    private Executor executor = new SimpleExecutor();

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {
        return executor.query(configuration, configuration.getMapperStatementMap().get(statementId), params);
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);
        if (objects.size() == 1) {
            return (T) objects.get(0);
        } else {
            throw new RuntimeException("返回结果为null或者结果过多！");
        }
    }

    @Override
    public int insert(String statementId, Object... params) throws Exception {
        return executor.update(configuration, configuration.getMapperStatementMap().get(statementId), params);
    }

    @Override
    public int update(String statementId, Object... params) throws Exception {
        return executor.update(configuration, configuration.getMapperStatementMap().get(statementId), params);
    }

    @Override
    public int delete(String statementId, Object... params) throws Exception {
        return executor.update(configuration, configuration.getMapperStatementMap().get(statementId), params);
    }


    @Override
    public <T> T getMapper(Class<?> mapperClazz) throws Exception {
        //JDK动态代理 proxy 代理类 method 代理的方法 args 入参
        return (T) Proxy.newProxyInstance(mapperClazz.getClassLoader(), new Class[]{mapperClazz}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //执行方法名 <=> id
                String methodName = method.getName();
                //代理类的全限定名 <=> namespace
                String name = method.getDeclaringClass().getName();
                //唯一标识
                String statementId = name + "." + methodName;
                MapperStatement mapperStatement = configuration.getMapperStatementMap().get(statementId);
                MapperType mapperType = mapperStatement.getMapperType();
                if (MapperType.SELECT == mapperType) {
                    //判断是否实现泛型类型参数化
                    if (method.getGenericReturnType() instanceof ParameterizedType) {
                        return selectList(statementId, args);
                    } else {
                        return selectOne(statementId, args);
                    }
                } else if (MapperType.INSERT == mapperType) {
                    return insert(statementId, args);
                } else if (MapperType.UPDATE == mapperType) {
                    return update(statementId, args);
                } else if (MapperType.DELETE == mapperType) {
                    return delete(statementId, args);
                } else {
                    throw new RuntimeException("暂不支持的操作类型");
                }
            }
        });
    }

    @Override
    public void close() throws SQLException {
        executor.closeConnection();
    }
}
