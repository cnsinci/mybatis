package com.lagou.enums;

public enum MapperType {
    /**
     * 查询
     */
    SELECT,
    /**
     * 新增
     */
    INSERT,
    /**
     * 修改
     */
    UPDATE,
    /**
     * 删除
     */
    DELETE;
}
