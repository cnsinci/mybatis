package com.lagou.executor;

import com.lagou.pojo.BoundSql;
import com.lagou.pojo.Configuration;
import com.lagou.pojo.MapperStatement;
import com.lagou.utils.GenericTokenParser;
import com.lagou.utils.ParameterMapping;
import com.lagou.utils.ParameterMappingTokenHandler;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @date 2020/12/10 15:06
 */
public class SimpleExecutor implements Executor {
    private Connection connection = null;

    @Override
    public <E> List<E> query(Configuration configuration, MapperStatement mapperStatement, Object[] params) throws SQLException, NoSuchFieldException, IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException {
        //获取连接
        connection = configuration.getDataSource().getConnection();
        //获取配置文件中的sql
        String sql = mapperStatement.getSql();
        //替换占位符，并收集占位符处的属性字段名称
        BoundSql boundSql = getBoundSql(sql);
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSql());
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        Class<?> paramClazz = mapperStatement.getParamClazz();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            //#{usename} 此处content就是usename
            String content = parameterMapping.getContent();
            //从入参类中 获取 username 属性对象
            Field declaredField = paramClazz.getDeclaredField(content);
            //暴力破解
            declaredField.setAccessible(true);
            // 从对象params[0]中获取对应的属性值
            Object o = declaredField.get(params[0]);

            preparedStatement.setObject(i + 1, o);
        }
        ResultSet resultSet = preparedStatement.executeQuery();
        Class<?> resultClazz = mapperStatement.getResultClazz();
        List<Object> resultList = new ArrayList<>();
        while (resultSet.next()) {
            Object result = resultClazz.newInstance();
            //获取查询结果的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取有多少列
            int columnCount = metaData.getColumnCount();
            for (int i = 1; i < columnCount; i++) {
                //拿到列名
                String columnName = metaData.getColumnName(i);
                //在返回的结果里取对应列的值
                Object value = resultSet.getObject(columnName);
                //为类创建属性的读写描述器，获取读写方法
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnName, resultClazz);
                //获取写方法
                Method writeMethod = propertyDescriptor.getWriteMethod();
                //向类的实例中写入对应结果的值
                writeMethod.invoke(result, value);
            }
            resultList.add(result);
        }
        return (List<E>) resultList;
    }

    @Override
    public <T> int update(Configuration configuration, MapperStatement mapperStatement, Object[] params) throws SQLException, NoSuchFieldException, IllegalAccessException {
        //获取连接
        connection = configuration.getDataSource().getConnection();
        //获取配置文件中的sql
        String sql = mapperStatement.getSql();
        //替换占位符，并收集占位符处的属性字段名称
        BoundSql boundSql = getBoundSql(sql);
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSql());
        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        Class<?> paramClazz = mapperStatement.getParamClazz();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            //#{usename} 此处content就是usename
            String content = parameterMapping.getContent();
            //从入参类中 获取 username 属性对象
            Field declaredField = paramClazz.getDeclaredField(content);
            //暴力破解
            declaredField.setAccessible(true);

            // 从对象params[0]中获取对应的属性值
            Object o = declaredField.get(params[0]);
            preparedStatement.setObject(i + 1, o);
        }
        return preparedStatement.executeUpdate();
    }

    /**
     * 替换sql的占位符 并且记录对应位置的值
     *
     * @param sql
     * @return
     */
    private BoundSql getBoundSql(String sql) {
        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", parameterMappingTokenHandler);
        String parseSql = genericTokenParser.parse(sql);
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();
        BoundSql boundSql = new BoundSql();
        boundSql.setSql(parseSql);
        boundSql.setParameterMappingList(parameterMappings);
        return boundSql;
    }

    @Override
    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }
}
