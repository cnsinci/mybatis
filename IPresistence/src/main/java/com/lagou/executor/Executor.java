package com.lagou.executor;

import com.lagou.pojo.Configuration;
import com.lagou.pojo.MapperStatement;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Sinci.Wang
 * @date 2020/12/10 15:03
 */
public interface Executor {
    /**
     * 执行查询方法
     */
    <E> List<E> query(Configuration configuration, MapperStatement mapperStatement, Object[] params) throws SQLException, NoSuchFieldException, IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException;

    <T> int update(Configuration configuration, MapperStatement mapperStatement, Object[] params) throws SQLException, NoSuchFieldException, IllegalAccessException;

    void closeConnection() throws SQLException;
}
